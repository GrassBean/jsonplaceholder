from utils.user_utils import *
from data.users_data import *
from constant.url_constant import *


def test_find_a_user_with_invalid_searching_key():
    search_user(SEARCHING_URL, USER_SEARCHING_INVALID_KEY, USER_SEARCHING_INVALID_RESULT)
