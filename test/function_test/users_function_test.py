from utils.json_utils import *
from utils.user_utils import *
from data.users_data import *
from constant.url_constant import *
import requests


def test_present_of_users_list():
    json_data = get_json(USER_URL)
    names_list = get_all_names_in_json_data(json_data)
    expected_list = USERS_NAME
    compare_names_of_json_data_with_expected_data(names_list, expected_list)


def test_find_a_user():
    search_user(SEARCHING_URL, USER_SEARCHING_KEY, USER_SEARCHING_RESULT)


def test_create_a_user():
    response = requests.request("POST", USER_URL, data=USER_ADDING)
    check_user_exist(response, 'name', USER_ADDING_RESULT)


def test_update_a_user():
    response = requests.request("PATCH", USER_URL + USER_UPDATE_KEY, data=USER_UPDATING)
    check_user_exist(response, 'name', USER_UPDATING_RESULT)


def test_delete_a_user():
    response = requests.request("DELETE", USER_URL + USER_DELETING_KEY)
    print(response.text)
    check_user_is_deleted(response, USER_DELETING_RESULT)
