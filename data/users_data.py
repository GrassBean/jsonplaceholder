USERS_NAME = ["Leanne Graham", "Ervin Howell", "Clementine Bauch", "Patricia Lebsack", "Chelsey Dietrich",
              "Mrs. Dennis Schulist", "Kurtis Weissnat", "Nicholas Runolfsdottir V", "Glenna Reichert",
              "Clementina DuBuque"]

USER_SEARCHING_RESULT = ["Leanne Graham"]
USER_SEARCHING_KEY = "?name=Leanne%20Graham"
USER_SEARCHING_INVALID_KEY = "?name=Leanne"
USER_SEARCHING_INVALID_RESULT = ""

USER_ADDING = {
    "name": "ADDING NAME",
    "username": "USER_NAME",
    "email": "Sincere@april.biz",
    "address": {
        "street": "Kulas Light",
        "suite": "Apt. 556",
        "city": "Gwenborough",
        "zipcode": "92998-3874",
        "geo": {
            "lat": "-37.3159",
            "lng": "81.1496"
        }
    }
}
USER_ADDING_RESULT = "ADDING NAME"

USER_UPDATE_KEY = "1"
USER_UPDATING = {
    "name": "UPDATE NAME"
}
USER_UPDATING_RESULT = "UPDATE NAME"

USER_DELETING_KEY = "2"
USER_DELETING_RESULT = "{}"
