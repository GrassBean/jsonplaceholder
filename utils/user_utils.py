import logging
from utils.json_utils import *


def get_all_names_in_json_data(json_data):
    names_list = []
    if len(json_data) == 1:
        names_list.append(json_data[0]['name'])
    else:
        for i in range(len(json_data)):
            names_list.append(json_data[i]['name'])

    return names_list


def compare_names_of_json_data_with_expected_data(names_list, expected_list):
    counter_of_name_list = len(names_list)
    counter_of_expected_list = len(expected_list)
    if counter_of_name_list != counter_of_expected_list:
        logging.getLogger().warning("users list are loaded wrong")
    else:
        for i in range(counter_of_expected_list):
            assert (names_list[i] == expected_list[i])


def check_user_exist(response_string, item_checking, expected):
    assert ((response_string.json())[item_checking] == expected)


def check_user_is_deleted(response_string, expected):
    assert (response_string.text == expected)


def search_user(searching_url, searching_key, searching_result):
    json_data = get_json(searching_url + searching_key)
    name_get_from_json = get_all_names_in_json_data(json_data)
    expected_list = searching_result
    compare_names_of_json_data_with_expected_data(name_get_from_json, expected_list)
