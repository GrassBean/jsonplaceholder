import json, urllib.request
import requests


def get_json(url_string):
    response = urllib.request.urlopen(url_string)
    json_data = json.loads(response.read().decode())
    return json_data


def get_response(method, url, data):
    return requests.request(method, url, data)
